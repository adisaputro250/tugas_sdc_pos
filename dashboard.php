<?php 
@session_start();

if(
    @$_SESSION['is_login'] == null  &&  
    @$_SESSION['is_login'] == false
    ){
    header('location: login.php');
} 

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>DASHBOARD</title>

  <script src="assets/js/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

  <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
</body>
<!-- <?php echo " ini halaman dashboardnya " .@$_SESSION['level'].". selamat datang ".@$_SESSION['nama'];?>
<a href="logout.php">
<button type="button" class="btn btn-danger">Keluar</button> -->
</a> 

<div class="navigator">
  
  <nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Sistem kepegawaian</a>
    </div>
  
    <?php
    switch (@$_SESSION['level']){
      case'pimpinan':
        include "template/navigation/nav_pimpinan.php";
      break;
      case'personalia':
        include "template/navigation/nav_personalia.php";
      break;
      case'user':
        include "template/navigation/nav_user.php";
      break;
      default:
    break;
    }
    ?>
  </nav>

</div>
</body>


</html>